# Проект «Прогнозирование временных рядов»

_Предлагаем вашему вниманию проектную работу **Максима Саблева**, выпускника [курса по SRE](https://otus.ru/lessons/sre/), в которой было выполнено прогнозирование временного ряда (на примере температуры воздуха) при помощи библиотеки Prophet, обеспечена загрузка в Prometheus и отображение на панели Grafana фактических и прогнозных значений временного ряда. Кроме того, была решена возникшая в ходе работы задача по исследованию влияния параметров запуска Prometheus на retention-политику._

## Цели и задачи

Цель курсовой работы — применить практики и инструменты SRE, освоенные на курсе, для решения следующих задач:

- прогнозирование временного ряда при помощи Prophet,
- загрузка фактических и прогнозных значений в Prometheus,
- создание дэшборда Grafana и отображение на нём загруженных фактических и прогнозных значений.

## Инструменты и технологии

- **Python** ([https://www.python.org/](https://www.python.org/)) — высокоуровневый язык программирования. Используется в проекте для создания Jupyter-ноутбуков.
- **Jupyter** ([https://jupyter.org/](https://jupyter.org/)) — среда интерактивного программирования; позволяет создавать и исполнять документы, содержащие исходный код на разных языках программирования, результаты исполнения этого кода, а также форматированный текст. Используется в проекте для создания и исполнения Jupyter-ноутбуков, реализующих всю логику работы.
- **Pandas** ([https://pandas.pydata.org/](https://pandas.pydata.org/)) — библиотека для анализа и работы с данными. Используется в проекте для загрузки, преобразования и сохранения данных.
- **Psycopg** ([https://www.psycopg.org/](https://www.psycopg.org/)) — PostgreSQL-адаптер для Python. Используется в проекте для подключения Jupyter-ноутбуков к СУБД PostgreSQL.
- **Prophet** ([https://facebook.github.io/prophet/](https://facebook.github.io/prophet/)) — библиотека для прогнозирования временных рядов; на основе обучающей выборки тренирует модель машинного обучения, которая прогнозирует последующие значения временного ряда. Используется в проекте для прогнозирования временного ряда среднесуточных значений температуры воздуха.
- **PostgreSQL** ([https://www.postgresql.org/](https://www.postgresql.org/)) — реляционная СУБД. Используется в проекте для хранения подготовленного массива данных наблюдений за погодой.
- **pgAdmin** ([https://www.pgadmin.org/](https://www.pgadmin.org/)) — веб-интерфейс СУБД PostgreSQL. Используется в проекте для взаимодействия разработчика с БД.
- **Prometheus** ([https://prometheus.io/](https://prometheus.io/)) — программное обеспечение для мониторинга событий и оповещения. Используется в проекте как база данных временных рядов (Time Series Database, TSDB) для загрузки и хранения ретроспективных фактических и прогнозных значений среднесуточной температуры.
- **Grafana** ([https://grafana.com/grafana/](https://grafana.com/grafana/)) — программное обеспечение для аналитики и интерактивной визуализации. Используется в проекте для визуализации значений временных рядов фактических и прогнозных значений среднесуточной температуры.
- **Docker** ([https://www.docker.com/](https://www.docker.com/)) — система автоматизации развёртывания и управления приложениями в средах контейнерной виртуализации. Используется в проекте для локального запуска контейнеров.
- **Docker compose** ([https://docs.docker.com/compose/](https://docs.docker.com/compose/)) — инструмент описания и запуска многоконтейнерных приложений. Используется в проекте для локального развёртывания приложения, состоящего из СУБД PostgreSQL, веб-интерфейса СУБД pgAdmin, системы мониторинга Prometheus и системы визуализации метрик Grafana.
- **Git** ([https://git-scm.com/](https://git-scm.com/)) — распределённая система контроля версий. Используется в проекте для управления исходными кодами проекта.
- **GitLab** ([https://gitlab.com/](https://gitlab.com/)) — платформа управления жизненным циклом ПО, включающая системы управления репозиториями исходных кодов, задачами и инцидентами, CI/CD-конвейерами. Используется в проекте для хранения исходных кодов проекта и предоставления к ним доступа.
- **VS Code** ([https://code.visualstudio.com/](https://code.visualstudio.com/)) — текстовый редактор. Используется в проекте для работы с исходными кодами проекта.
- **PlantUML** ([https://plantuml.com/](https://plantuml.com/)) — инструмент создания диаграмм. Используется в проекте для создания диаграмм в документации решения.

## Часть 1. Прогнозирование временных рядов, загрузка метрик в Prometheus и отображение в Grafana

Репозиторий с исходными кодами (не включает массив данных наблюдений за погодой из-за его больших размеров):
[https://gitlab.com/sablev/otus-sre-course-work](https://gitlab.com/sablev/otus-sre-course-work)

План работ:

- добыть массив данных наблюдений за погодой,
- подготовить данные для целей прогнозирования,
- выбрать данные для обучения модели и проверки прогноза,
- обучить модель Prophet и выполнить прогнозирование,
- сформировать временные ряды фактических и прогнозных значений,
- преобразовать временные ряды в формат метрик Prometheus и загрузить в TSDB,
- создать дэшборд Grafana, отражающий загруженные метрики.

### Шаг 1. Добыча массива данных наблюдений за погодой

В качестве источника данных о наблюдениях за погодой выбран массив данных, предоставляемый [Всероссийским Научно-исследовательским институтом Гидрометеорологической Информации — Мировым центром данных](http://aisori-m.meteo.ru/waisori/index0.xhtml).

Массив данных содержит:

- свыше 19 млн суточных наблюдений погоды от 600 метеостанций за всю историю наблюдений,
- данные о минимальной, средней, максимальной среднесуточной температуре,
- данные об уровне осадков,
- показатели качества данных о температуре и осадках.

В курсовой работе используются данные о среднесуточной температуре воздуха.

### Шаг 2. Подготовка данных для целей прогнозирования

План работ:

- преобразовать данные в пригодный для обработки формат,
- очистить данные от ошибочных сведений,
- спроектировать БД для загрузки данных,
- подготовить данные к загрузке в БД,
- загрузить данные в БД для удобства анализа.

Структура БД:

```plantuml
entity countries {
    * id : int
    --
    * name : varchar(64)
}

entity stations {
    * id : int
    --
    * country_id : int
    * name : varchar(128)
}

entity observations {
    * station_id : int
    * created_at : date
    --
    * tflag : quality_level
    * qtmin : quality_level
    * qtmean : quality_level
    * qtmax : quality_level
    * cr : precipitation_feature
    * qr : quality_level
    tmin : numeric(5,1)
    tmean : numeric(5,1)
    tmax : numeric(5,1)
    r : numeric(5,1)
}

countries ||--o{ stations
stations ||--o{ observations
```

DDL-скрипт:

```sql
CREATE TYPE quality_level AS enum ('true', 'mismatched', 'false');
CREATE TYPE precipitation_feature AS enum ('true', 'multiple days', 'zero', 'few', 'false');

CREATE TABLE countries(
    id INT PRIMARY KEY,
    name VARCHAR(64) NOT NULL
);

COMMENT ON TABLE countries IS 'Страны';
COMMENT ON COLUMN countries.id IS 'Идентификатор страны';
COMMENT ON COLUMN countries.name IS 'Название страны';

CREATE TABLE stations(
    id INT PRIMARY KEY,
    country_id INT NOT NULL,
    name VARCHAR(128) NOT NULL
);

COMMENT ON TABLE stations IS 'Метеостанции';
COMMENT ON COLUMN stations.id IS 'Идентификатор метеостанции';
COMMENT ON COLUMN stations.country_id IS 'Ссылка на страну';
COMMENT ON COLUMN stations.name IS 'Название метеостанции';

CREATE TABLE observations(
    station_id INT NOT NULL,
    created_at DATE NOT NULL,
    tflag quality_level NOT NULL,
    tmin NUMERIC(5, 1),
    qtmin quality_level NOT NULL,
    tmean NUMERIC(5, 1),
    qtmean quality_level NOT NULL,
    tmax NUMERIC(5, 1),
    qtmax quality_level NOT NULL,
    r NUMERIC(5, 1),
    cr precipitation_feature NOT NULL,
    qr quality_level NOT NULL,
    PRIMARY KEY(station_id, created_at)
);

COMMENT ON TABLE observations IS 'Наблюдения';
COMMENT ON COLUMN observations.station_id IS 'Идентификатор метеостанции';
COMMENT ON COLUMN observations.created_at IS 'Дата наблюдения';
COMMENT ON COLUMN observations.tflag IS 'Групповой признак качества для показателей температуры воздуха';
COMMENT ON COLUMN observations.qtmin IS 'Признак качества минимальной температуры воздуха за сутки';
COMMENT ON COLUMN observations.qtmean IS 'Признак качества среднесуточной температуры воздуха';
COMMENT ON COLUMN observations.qtmax IS 'Признак качества максимальной температуры воздуха за сутки';
COMMENT ON COLUMN observations.tmin IS 'Минимальная температура воздуха за сутки';
COMMENT ON COLUMN observations.tmean IS 'Среднесуточная температура воздуха';
COMMENT ON COLUMN observations.tmax IS 'Максимальная температура воздуха за сутки';
COMMENT ON COLUMN observations.r IS 'Суточная сумма осадков';
COMMENT ON COLUMN observations.cr IS 'Дополнительный признак суточной суммы осадков';
COMMENT ON COLUMN observations.qr IS 'Признак качества суточной суммы осадков';
```

Справочник `quality_level`:

| Код | Значение | Описание |
| --- | --- | --- |
| 0 | 'true' | Значение достоверно |
| 1 | 'mismatched' | Значение не согласуется с данными архива срочных наблюдений |
| 9 | 'false' | Значение забраковано или наблюдения не проводились |

Справочник `precipitation_feature`:

| Код | Значение | Описание |
| --- | --- | --- |
| 0 | 'true' | Измеренное количество осадков 0,1 мм и более |
| 1 | 'multiple days' | Осадки измерены за несколько дней |
| 2 | 'zero' | Измерения осадков производились, но осадков не было (R = 0) |
| 3 | 'few' | Наблюдались только следы осадков (< 0,1 мм) (R = 0) |
| 9 | 'false' | Значение забраковано или наблюдения не проводились |

Jupyter-ноутбуки, реализующие шаги преобразования данных, вплоть до их загрузки в БД:

| Наименование | Описание |
| --- | --- |
| [1-prepare_stations_data.ipynb](https://gitlab.com/sablev/otus-sre-course-work/-/blob/main/solution/1-prepare_stations_data.ipynb) | Подготовка сведений о метеостанциях |
| [2-prepare_ts_data.ipynb](https://gitlab.com/sablev/otus-sre-course-work/-/blob/main/solution/2-prepare_ts_data.ipynb) | Подготовка сведений о наблюдениях погоды |
| [3-prepare_data_to_db.ipynb](https://gitlab.com/sablev/otus-sre-course-work/-/blob/main/solution/3-prepare_data_to_db.ipynb) | Подготовка сведений к загрузке в БД |
| [4-load_data_to_db.ipynb](https://gitlab.com/sablev/otus-sre-course-work/-/blob/main/solution/4-load_data_to_db.ipynb) | Загрузка сведений в БД |

### Шаг 3. Выбор данных для обучения модели и проверки прогноза

План работ:

- проанализировать качество данных,
- выбрать данные для использования в качестве обучающей и проверочной выборки.

Теперь, когда данные загружены в БД и их удобно анализировать, необходимо найти наиболее качественные данные — не имеющие пропусков и обладающие наилучшим качеством. Эти данные и будут в дальнейшем использоваться для обучения модели.

Сколько всего наблюдений в год по каждой метеостанции?

```sql
SELECT
    station_id,
    extract(year from created_at) AS year,
    COUNT(*) AS count
FROM
    observations
GROUP BY
    station_id,
    year
ORDER BY
    station_id,
    year;
```

У каких метеостанций и в какие годы наибольшее кол-во качественных наблюдений?

```sql
SELECT
    station_id,
    extract(year from created_at) AS year,
    COUNT(*) AS count
FROM
    observations
WHERE
    tflag != 'true'
GROUP BY
    station_id,
    year
ORDER BY
    count ASC,
    year DESC;
```

Одни из самых качественных данных температуры — на метеостанции `Восточная`:

```sql
SELECT * FROM stations WHERE id = 24679;
```

За какие годы данные о наблюдениях температуры на станции `Восточная` имеют наивысшее качество?

```sql
SELECT
    station_id,
    extract(year from created_at) AS year,
    COUNT(*) AS count
FROM
    observations
WHERE
    station_id = 24679
    AND tflag = 'true'
GROUP BY
    station_id,
    year
ORDER BY
    year DESC,
    count DESC;
```

Запрос данных для обучения модели (фактическая среднесуточная температура воздуха за 1997-2020 гг.):

```sql
SELECT
    created_at,
    tmean
FROM observations
WHERE
    station_id = 24679
    AND extract(year from created_at) >= 1997
    AND extract(year from created_at) <= 2020;
```

Запрос данных для проверки качества обучения модели (фактическая среднесуточная температура воздуха за 2021 г.):

```sql
SELECT
    created_at,
    tmean
FROM observations
WHERE
    station_id = 24679
    AND extract(year from created_at) == 2021;
```

### Шаг 4. Обучение модели Prophet, прогнозирование и экспорт временных рядов фактических и прогнозных значений

[5-learn.ipynb](https://gitlab.com/sablev/otus-sre-course-work/-/blob/main/solution/5-learn.ipynb)

### Шаг 5. Подготовка метрик и загрузка в Prometheus

План работ:

- проектирование метрик (выбор наименования, типа метрики, параметров и их значений),
- формирование файла с метриками в openmetrics-формате,
- загрузка метрик в Prometheus.

Температура день ото дня может как увеличиваться, так и уменьшаться, поэтому метрике подходит тип `gauge`:

| № | Наименование | Тип | Описание |
| --- | --- | --- | --- |
| 1 | air_temperature_celsius | gauge | Среднесуточное значение температуры воздуха в градусах Цельсия |

Параметры:

| Наименование | Описание                   | Примеры значений                                                        |
|--------------|----------------------------|------------------------------------------------------------------------|
| kind         | Вид значения               | `min` — минимальное, ``mean`` — среднее, `max` — максимальное                  |
| type         | Тип значения               | `observation` — наблюдение, `forecast` — прогноз                           |
| station_id   | Идентификатор метеостанции | `24679`                                                                  |
| source       | Источник сведений          | `meteo_ru` — для наблюдений, `prophet_defaults` — для прогнозных значений  |

Для загрузки в Prometheus ретроспективных значений может использоваться утилита `promtool`. Утилита позволяет загружать значения из текстовых файлов в в openmetrics-формате.

Пример команды для загрузки метрик в TSDB:

```console
promtool tsdb create-blocks-from openmetrics /openmetrics/24679.txt /prometheus/data
```

Пример файла в openmetrics-формате:

```plain
# HELP air_temperature_celsius Daily air temperature in Celsius degrees.
# TYPE air_temperature_celsius gauge
air_temperature_celsius{kind="mean",type="observation",station_id="24679",source="meteo_ru"} -37.5 1609459200
air_temperature_celsius{kind="mean",type="forecast",station_id="24679",source="prophet_defaults"} -33.7 1640908800
# EOF
```

Преобразование временных рядов значений в openmetrics-формат для загрузки в Prometheus реализовано в Jupyter-ноутбуке [6-backfill_metrics.ipynb](https://gitlab.com/sablev/otus-sre-course-work/-/blob/main/solution/6-backfill_metrics.ipynb). Результаты преобразования, метрики в openmetrics-формате — [24679.txt](https://gitlab.com/sablev/otus-sre-course-work/-/blob/main/solution/openmetrics/24679.txt)

### Шаг 6. Создание дэшборда Grafana

Создание дэшборда реализовано с использованием библиотеки `grafanalib` в Jupyter-ноутбуке [7-grafana-dashboard.ipynb](https://gitlab.com/sablev/otus-sre-course-work/-/blob/main/solution/7-grafana-dashboard.ipynb). Загрузка дэшборда в Grafana осуществляется через Grafana Dashboard API.

На дэшборде отображаются:

- графики среднесуточной температуры по результатам наблюдений и прогнозные за период,
- минимальное, среднее и максимальное значения за период.

![Дэшборд в Grafana](./assets/dashboard.png "Дэшборд в Grafana")

### Выводы

### _Направления развития

1. Обеспечить лучшее качество предсказаний:
   1. Использовать настройки
   2. Использовать дополнительные средства
2. Попробовать другие библиотеки для предсказания временных рядов

## _Часть 2. Исследование влияния параметров запуска Prometheus на retention-политику

Репозиторий с исходными кодами:
[https://gitlab.com/sablev/prometheus-retention-test](https://gitlab.com/sablev/prometheus-retention-test)

[test.ipynb](https://gitlab.com/sablev/prometheus-retention-test/-/blob/main/test.ipynb)
